import React from 'react';
import './style.scss'
const Buttons = props => {
    return (
        <div className={"decide-btn"} text={props.text} style={{backgroundColor: props.color}} onClick={props.buttonPressed}>
            {props.text}
        </div>
    );
};

export default Buttons;