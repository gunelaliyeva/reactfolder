import React from 'react';
import './style.scss'
const Header = props => {
    return (
        <div headertext={props.headertext}  className={"header"}>
            <p className={"paragraph"}>{props.headerText}</p>
            <div className="close" onClick={props.closePressed}>X</div>
        </div>
    );
};

export default Header;