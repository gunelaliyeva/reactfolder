import React, {Component} from 'react';
import ModalWindow from "./modalWindow";
import Buttons from "./buttons";

class Main extends Component {
    state={
        modalWindow1: false,
        modalWindow2: false
    }
    openFirst=()=>{
        this.setState({modalWindow1:!this.state.modalWindow1});
    }

    openSecond=()=>{
        this.setState({modalWindow2:!this.state.modalWindow2});
    }
    render() {
        return (
            <div className="App" style={{backGroundColor:this.props.bgcolor}}>

                <Buttons text={"Open first modal"} color={"green"} buttonPressed={this.openFirst}/>

                <Buttons text={"Open second modal"} color={"green"} buttonPressed={this.openSecond}/>
                {
                    this.state.modalWindow1?<ModalWindow header={"Do you want to delete this file?"} paragraph={"Once you delete this file, it won’t be possible to undo this action. \n" +
                    "Are you sure you want to delete it?"} visible={this.state.modalWindow1} />:null
                }
                {
                    this.state.modalWindow2?<ModalWindow header={"Do you want to delete this file?"} paragraph={"Do you want to delete this file?"} visible={this.state.modalWindow2}/>:null
                }
            </div>
        );
    }

}

export default Main;
